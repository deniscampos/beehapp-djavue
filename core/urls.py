from core import views
from django.urls import path

urlpatterns = [
    path('api/dapau', views.dapau),
    path('api/login', views.login),
    path('api/logout', views.logout),
    path('api/whoami', views.whoami),
    path('api/settings', views.settings),
    path('api/add_specie', views.add_specie),
    path('api/add_swarm', views.add_swarm),
    path('api/list_swarms', views.list_swarms),
    path('api/list_species', views.list_species),
    path('api/specie', views.get_specie),
    path('api/add_user', views.add_user),
]
