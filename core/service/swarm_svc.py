from core.models import Swarm, Specie


def add_swarm(user, specie_id, date_capture):
    specie = Specie.objects.get(id=specie_id)
    swarm = Swarm.objects.create(user=user, specie=specie, date_capture=date_capture)
    return swarm.to_dict_json()


def list_swarms(loggeduser):
    swarms = Swarm.objects.filter(user=loggeduser)
    return [swarm.to_dict_json() for swarm in swarms]

