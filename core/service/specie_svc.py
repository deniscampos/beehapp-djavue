from core.models import Specie


def add_specie(name, description):
    specie = Specie.objects.create(name=name, description=description)
    return specie.to_dict_json()


def get_specie(specie_id):
    specie = Specie.objects.get(id=specie_id)
    return specie.to_dict_json()


def list_species():
    species = Specie.objects.all()
    return [specie.to_dict_json() for specie in species]