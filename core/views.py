# coding: utf-8

from django.http.response import HttpResponse, JsonResponse
from django.contrib import auth
from commons.django_views_utils import ajax_login_required
from core.forms import SignUpForm
from core.service import log_svc, globalsettings_svc, swarm_svc, specie_svc
from django.views.decorators.csrf import csrf_exempt


def dapau(request):
    raise Exception('break on purpose')


@csrf_exempt
def login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = auth.authenticate(username=username, password=password)
    user_dict = None
    if user is not None:
        if user.is_active:
            auth.login(request, user)
            log_svc.log_login(request.user)
            user_dict = _user2dict(user)
    return JsonResponse(user_dict, safe=False)


def logout(request):
    if request.method.lower() != 'post':
        raise Exception('Logout only via post')
    if request.user.is_authenticated:
        log_svc.log_logout(request.user)
    auth.logout(request)
    return HttpResponse('{}', content_type='application/json')


def whoami(request):
    i_am = {
        'user': _user2dict(request.user),
        'authenticated': True,
    } if request.user.is_authenticated else {'authenticated': False}
    return JsonResponse(i_am)


def settings(request):
    le_settings = globalsettings_svc.list_settings()
    return JsonResponse(le_settings)

@csrf_exempt
def add_user(request):
    form = SignUpForm(request.POST)
    user_dict = None
    if form.is_valid():
        form.save()
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password1')
        user = auth.authenticate(username=username, password=password)
        if user is not None and user.is_active:
            auth.login(request, user)
            log_svc.log_login(request.user)
            user_dict = _user2dict(user)
        return JsonResponse(user_dict, safe=False)
    return JsonResponse({'errors': form.errors})

@ajax_login_required
def add_specie(request):
    name = request.POST['name']
    description = request.POST['description']
    specie = specie_svc.add_specie(name, description)
    return JsonResponse(specie)


def list_species(request):
    species = specie_svc.list_species()
    return JsonResponse({'data': species})


def get_specie(request):
    specie_id = request.GET['specieId']
    specie = specie_svc.get_specie(specie_id)
    return JsonResponse({'data': specie})


@ajax_login_required
def list_swarms(request):
    swarms = swarm_svc.list_swarms(request.user)
    return JsonResponse({'data': swarms})


@ajax_login_required
def add_swarm(request):
    specie_id = request.POST['specieId']
    date_capture = request.POST['dateCapture']
    swarm = swarm_svc.add_swarm(request.user, specie_id, date_capture)
    return JsonResponse(swarm)


def _user2dict(user):
    d = {
        'id': user.id,
        'name': user.get_full_name(),
        'username': user.username,
        'first_name': user.first_name,
        'last_name': user.last_name,
        'email': user.email,
        'permissions': {
            'ADMIN': user.is_superuser,
            'STAFF': user.is_staff,
        }
    }
    return d
