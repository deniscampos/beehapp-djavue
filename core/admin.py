from django.contrib import admin

from core.models import ActivityLog, Specie


class ActivityLogAdmin(admin.ModelAdmin):
    list_display = ('type', 'logged_user', 'created_at')


class SpecieAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')


admin.site.register(ActivityLog, ActivityLogAdmin)
admin.site.register(Specie, SpecieAdmin)
