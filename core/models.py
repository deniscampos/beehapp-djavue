import json

from django.db import models
from django.contrib.auth.models import User


class ActivityLog(models.Model):
    type = models.CharField(max_length=64)
    logged_user = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL)
    fromuser = models.ForeignKey(User, null=True, blank=True, related_name="activitylogs_withfromuser", on_delete=models.SET_NULL)
    jsondata = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField('criado em', auto_now_add=True)

    class Meta:
        ordering = ('-created_at',)

    def __str__(self):
        return '%s / %s / %s' % (
            self.type,
            self.logged_user,
            self.created_at,
        )


class Specie(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=512)

    def to_dict_json(self):
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description,
        }


class Swarm(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    specie = models.ForeignKey(Specie, on_delete=models.PROTECT)
    date_capture = models.DateField()

    def to_dict_json(self):
        return {
            'id': self.id,
            'dateCapture': self.date_capture,
            'specieName': self.specie.name
        }
