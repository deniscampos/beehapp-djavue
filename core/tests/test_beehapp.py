from core.models import User, Specie
from django.test.client import Client
from django.test.testcases import TestCase
from core.tests import fixtures
import json


class TestAuthApi(TestCase):
    @classmethod
    def setUpTestData(cls):
        fixtures.beekeepers()

    def test_swarms(self):
        denis, bruno, carlos, anyone = Client(), Client(), Client(), Client()
        denis.force_login(User.objects.get(username='denis'))
        bruno.force_login(User.objects.get(username='bruno'))
        carlos.force_login(User.objects.get(username='carlos'))
        anyone.force_login(User.objects.get(username='anyone'))
        self._add_specie(anyone, {'name': 'Jataí', 'description': 'Uma das espécies mais comuns, encontrada em todo o território brasileiro'})
        self._add_specie(anyone, {'name': 'Mirim Droryana', 'description': 'Uma espécie de abelha muito mansa, muito conhecida por sua entrada que é dividida em 2 buracos'})
        self._add_specie(anyone, {'name': 'Mandaçaia MQA', 'description': 'Abelha grande, preta com 3 ou 4 listras amarelas interrompidas no abdômen'})
        self._add_specie(anyone, {'name': 'Mandaçaia MQQ', 'description': 'Abelha grande, marrom com 4 listras amarelas contínuas no abdômen'})
        self._add_specie(anyone, {'name': 'Tubuna', 'description': 'Abelha preta com cerca de 6cm, muito defensiva.'})
        self._assert_species(anyone, ['Jataí', 'Mirim Droryana', 'Mandaçaia MQA', 'Mandaçaia MQQ', 'Tubuna'])
        self._add_swarm(denis, 1, '2019-01-08')
        self._add_swarm(denis, 2, '2020-01-08')
        self._add_swarm(denis, 3, '2020-01-26')
        self._add_swarm(bruno, 3, '2018-04-18')
        self._add_swarm(bruno, 4, '2018-07-30')
        self._add_swarm(carlos, 4, '2018-07-30')
        self._add_swarm(carlos, 5, '2018-07-30')
        self._assert_my_swarms(denis, ['Jataí', 'Mirim Droryana', 'Mandaçaia MQA'])
        self._assert_my_swarms(bruno, ['Mandaçaia MQA', 'Mandaçaia MQQ'])
        self._assert_my_swarms(carlos, ['Mandaçaia MQQ', 'Tubuna'])
        self._add_user(anyone, {'username': 'usuario1', 'first_name': 'usuario1', 'password1': '102938asd', 'password2': '102938asd'})
        self._add_user(anyone, {'username': 'usuario2', 'first_name': 'usuario2', 'password1': '102938asd', 'password2': '102938asd'})

    def _add_user(self, client, user):
        r = client.post('/api/add_user', user)
        self.assertEquals(200, r.status_code)
        data = json.loads(r.content.decode('utf-8'))
        self.assertIsNotNone(data)

    def _add_specie(self, client, specie):
        r = client.post('/api/add_specie', specie)
        self.assertEquals(200, r.status_code)
        data = json.loads(r.content.decode('utf-8'))
        self.assertIsNotNone(data)

    def _assert_species(self, client, expected_species):
        r = client.get('/api/list_species')
        self.assertEquals(200, r.status_code)
        species = json.loads(r.content.decode('utf-8'))['data']
        actual_species = [specie['name'] for specie in species]
        self.assertEquals(actual_species, expected_species)

    def _get_specie(self, client, specie_id):
        r = client.get('/api/specie', {'specieId': specie_id})
        self.assertEquals(200, r.status_code)
        return json.loads(r.content.decode('utf-8'))

    def _add_swarm(self, client, specie_id, capture_date):
        r = client.post('/api/add_swarm', {'specieId': specie_id, 'dateCapture': capture_date})
        self.assertEquals(200, r.status_code)
        data = json.loads(r.content.decode('utf-8'))
        self.assertIsNotNone(data)

    def _assert_my_swarms(self, client, expected_swarms):
        r = client.get('/api/list_swarms')
        self.assertEquals(200, r.status_code)
        swarms = json.loads(r.content.decode('utf-8'))['data']
        actual_swarms = [swarm['specieName'] for swarm in swarms]
        self.assertEquals(actual_swarms, expected_swarms)