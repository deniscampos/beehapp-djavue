import Vue from 'vue'
import Router from 'vue-router'
import Home from '~/pages/home.vue'
import Swarms from '~/pages/user/_username/index.vue'
import SwarmRegister from '~/pages/swarm-register.vue'
import UserRegister from '~/pages/user-register.vue'

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  routes: [
    { path: '/', component: Home, name: 'home' },
    { path: '/cadastro', component: UserRegister, name: 'user-register' },
    { path: '/enxames/:username', component: Swarms, name: 'swarms' },
    { path: '/enxames/cadastro', component: SwarmRegister, name: 'swarm-register' }
  ]
}

export function createRouter (ctx) {
  return new Router(routerOptions)
}
