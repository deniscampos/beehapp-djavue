import Vue from 'vue'
import VueFilters from 'vue-filters'
import moment from 'moment'

moment.locale('pt-br')
Vue.use(VueFilters)

Vue.filter('formatDate', (value, format = 'DD/MM/YYYY') => {
  if (!value) {
    return ''
  }

  if (typeof value === 'string') {
    value = moment(value)
  }

  return moment(value).format(format)
})
