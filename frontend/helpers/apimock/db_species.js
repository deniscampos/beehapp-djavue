export const species = {
  data: [
    { id: 1, name: 'Mandaçaia MQA', description: 'A Mandaçaia é uma abelha de clima ameno/frio, podendo ser encontrada em tonalidades marrom ou preta. A MQA possui listras amarelas interrompidas ao meio.' },
    { id: 2, name: 'Jataí', description: 'A Mandaçaia é uma abelha de clima ameno/frio, podendo ser encontrada em tonalidades marrom ou preta. A MQA possui listras amarelas interrompidas ao meio.' },
    { id: 3, name: 'Mirim Droriana', description: 'A Mandaçaia é uma abelha de clima ameno/frio, podendo ser encontrada em tonalidades marrom ou preta. A MQA possui listras amarelas interrompidas ao meio.' },
    { id: 4, name: 'Bugia', description: 'A Mandaçaia é uma abelha de clima ameno/frio, podendo ser encontrada em tonalidades marrom ou preta. A MQA possui listras amarelas interrompidas ao meio.' },
    { id: 5, name: 'Mandaçaia MQQ', description: 'A Mandaçaia é uma abelha de clima ameno/frio, podendo ser encontrada em tonalidades marrom ou preta. A MQA possui listras amarelas interrompidas ao meio.' },
    { id: 6, name: 'Tiuba', description: 'A Mandaçaia é uma abelha de clima ameno/frio, podendo ser encontrada em tonalidades marrom ou preta. A MQA possui listras amarelas interrompidas ao meio.' },
    { id: 7, name: 'Mirim preguiça', description: 'A Mandaçaia é uma abelha de clima ameno/frio, podendo ser encontrada em tonalidades marrom ou preta. A MQA possui listras amarelas interrompidas ao meio.' },
    { id: 8, name: 'Manduri', description: 'A Mandaçaia é uma abelha de clima ameno/frio, podendo ser encontrada em tonalidades marrom ou preta. A MQA possui listras amarelas interrompidas ao meio.' },
    { id: 9, name: 'Uruçu boca de renda', description: 'A Mandaçaia é uma abelha de clima ameno/frio, podendo ser encontrada em tonalidades marrom ou preta. A MQA possui listras amarelas interrompidas ao meio.' }
  ]
}
