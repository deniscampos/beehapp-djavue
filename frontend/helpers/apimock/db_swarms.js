export const swarms = {
  data: [
    { id: 1, specie: 'Mandaçaia MQA', dateCapture: '10/01/2020' },
    { id: 2, specie: 'Jataí', dateCapture: '10/01/2020' },
    { id: 3, specie: 'Mirim Droriana', dateCapture: '10/01/2020' },
    { id: 4, specie: 'Bugia', dateCapture: '10/01/2020' },
    { id: 5, specie: 'Mandaçaia MQQ', dateCapture: '10/01/2020' },
    { id: 6, specie: 'Jataí', dateCapture: '10/01/2020' },
    { id: 7, specie: 'Mirim Droriana', dateCapture: '10/01/2020' },
    { id: 8, specie: 'Tiuba', dateCapture: '10/01/2020' },
    { id: 9, specie: 'Mirim preguiça', dateCapture: '10/01/2020' },
    { id: 10, specie: 'Jataí', dateCapture: '10/01/2020' },
    { id: 11, specie: 'Manduri', dateCapture: '10/01/2020' },
    { id: 12, specie: 'Uruçu boca de renda', dateCapture: '10/01/2020' }
  ]
}
