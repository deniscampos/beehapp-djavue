import { zuck } from './db_people'
import { swarms } from './db_swarms'
import { species } from './db_species'
import { mockasync } from './mockutils'

const keepLoggedIn = true

export default {
  login (username, password) {
    return mockasync(zuck)
  },
  logout () {
    return mockasync({})
  },
  whoami () {
    const iam = { authenticated: keepLoggedIn }
    if (iam.authenticated) {
      iam.user = zuck
    }
    return mockasync(iam)
  },
  settings () {
    return mockasync({
      SENTRY_DSN_FRONT: ''
      // SENTRY_DSN_FRONT: 'https://abcd1234@sentry.example.com/10'
    })
  },
  listSwarm () {
    return mockasync(swarms)
  },
  addSwarm (swarm) {
    return mockasync({ data: swarm })
  },
  listSpecie () {
    return mockasync(species)
  },
  addUser (user) {
      return mockasync(user)
  }
}
