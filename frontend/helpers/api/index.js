import { get, post } from './ajaxutils'

export default {
  login (username, password) {
    return post('/api/login', { username, password })
  },
  logout () {
    return post('/api/logout')
  },
  whoami () {
    return get('/api/whoami')
  },
  settings () {
    return get('/api/settings')
  },
  listSpecie () {
    return get('/api/list_species')
  },
  listSwarm (username) {
    return get('/api/list_swarms', { username })
  },
  addSwarm (swarm) {
    return post('/api/add_swarm', swarm)
  },
  addUser (user) {
    return post('/api/add_user', user)
  }
}
